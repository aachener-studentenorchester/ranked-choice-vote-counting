# Voting System Script

This Python script implements a voting system that supports both Instant Runoff Voting (IRV) and Total Vote Runoff (TVR) through the use of Borda count. It is designed to process voting data from a CSV file, calculate scores based on the selected voting method, and determine the winner(s) of the election.

## Overview

The script comprises several key functions and a main block that orchestrates the overall voting process.

### Key Functions

#### `calculate_erststimmen_score(programme, stimmzettel)`
Calculates the normal first votes (Erststimmen) scores for a standard Instant Runoff Voting. It takes a dictionary of programs with their IDs and names, and a dictionary of ballots with voters' IDs and their ranked choices. It returns a dictionary of programs and their corresponding first vote scores.

#### `calculate_borda_score(programme, stimmzettel)`
Calculates the Borda score for Total Runoff Voting. Similar to `calculate_erststimmen_score`, it takes a dictionary of programs and a dictionary of ballots but calculates scores based on the Borda count method, where each position on a ballot is assigned a point value inversely related to its rank.

#### `runoff_round(programme, stimmzettel, scoring_function, results=set(), prior_eliminations=[])`
Performs a recursive runoff voting process. It takes the programs, ballots, a scoring function (either IRV or TVR), and optionally, the results and prior eliminations for recursion. It returns a set of tuples containing the winner's ID and the elimination order.

### Main Block

The main block handles input/output operations, initializes variables, and executes the voting process based on the provided CSV file and selected mode (IRV or TVR). It demonstrates how to parse command-line arguments for the input file and voting mode, load voting data from the file, prepare the data structures, and invoke the voting process to determine the winner.

### Usage

1. Prepare a CSV file containing the voting data. The file should include columns for option IDs, voter hashes, ranks, and option names.
2. Run the script with the following command-line options:
   - `-i <inputfile>`: Specifies the path to the input CSV file.
   - `-m <mode>`: Selects the voting mode, either `IRV` for Instant Runoff Voting or `TVR` for Total Vote Runoff. If not specified, `TVR` is used by default.
   - `-h`: Displays help information.

Example command:
```bash
python script_name.py -i path/to/inputfile.csv -m TVR
```

The script will load the data, calculate scores using the specified voting method, and print the winner(s) along with any necessary elimination order.

### Notes

The script is designed to handle ties by testing every possible elimination order in case of multiple candidates having the minimum score.
If multiple winners are possible due to different elimination orders, a warning is printed, indicating that ties may need to be broken manually.
