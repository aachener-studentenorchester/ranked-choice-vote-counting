import getopt
import os
import sys
import pandas as pd
from tabulate import tabulate

RED = '\033[91m'
RESET = '\033[0m'

DEFAULT_MODE = 'TVR'
mode = DEFAULT_MODE

def gauss_sum(n):
    """Summe der Zahlen von 1 bis n."""
    return n * (n+1) / 2

def calculate_erststimmen_score(programme, stimmzettel):
    """Normale Erststimmen Scores fuer normales Instant Runoff.
    :param programme: Dict in Form {ProgrammID: Programm_Name}. Name ist optional und kann null sein.
    :param stimmzettel: Dict in Form {'VoterID': [(Rang, ProgrammID)]}.
    :returns: Dict in Form: {ProgrammID: Erststimmen_Score}.
    """

    # Initialisiere Scores mit 0 in jeder Runde.
    scores = {key:0 for key in programme.keys()}
    
    # List Comprehension.
    # Ergebnis ist fuer jeden nicht leeren Stimmzettel das uebrige Programm mit dem hoechsten Ranking.
    for erststimme in [x[0][1] for x in 
            [sorted(zettel, key=lambda x: x[0]) for zettel in stimmzettel.values() if zettel != []]]: 

        #Zaehlt ausschliesslich Erststimmen.
        scores[erststimme] += 1
    return scores

def calculate_borda_score(programme, stimmzettel):
    """Borda Score fuer Total Runoff.
    :param programme: Dict in Form {ProgrammID: Programm_Name}.
    :param stimmzettel: Dict in Form {'VoterID': [(Rang, ProgrammID)]}.
    :returns: Dict in Form: {ProgrammID: Borda_Score}.
    """

    # Initialisiere Scores mit 0 in jeder Runde.
    scores = {key:0 for key in programme.keys()}

    # List Comprehension
    # Ergebnis ist fuer jeden Stimmzettel eine Version absteigend sortiert nach Ranking.
    for personal_ranking in [sorted(zettel, key=lambda x: x[0]) for zettel in stimmzettel.values()]:
        ranked_programme = [stimme[1] for stimme in personal_ranking] 
        unranked_programme = [programm for programm in programme.keys() if programm not in ranked_programme]

        score_for_unranked = gauss_sum(len(unranked_programme) - 1) / len(unranked_programme) if len(unranked_programme) > 0 else 0
        
        for i, stimme in enumerate(personal_ranking):
            programm_id = stimme[1]
            # Erste Wahl bekommt #Kandidaten-1 Punkte, zweite Kandidaten-2 etc.
            scores[programm_id] += len(programme) - i - 1

        for unranked_programm in unranked_programme:
            # Bei unvollständigen Stimmzetteln werden die nicht verteilte Punkte auf die nicht bewerteten Programme aufgeteilt.
            scores[unranked_programm] = scores[unranked_programm] + score_for_unranked

    return scores

def print_scores_table(scores_firstvote, scores_borda, programme, round_no, elimination_candidates=None):
    """Prints scores in a table format, with optional highlighting for elimination candidates."""
    headers = ['R', 'ID', 'Candidate', '1st', '%', 'Borda']
    table = []

    # Calculate the total number of first votes for the percentage calculation
    total_first_votes = sum(scores_firstvote.values())

    for option_id in sorted(scores_firstvote.keys()):
        bezeichnung = programme[option_id]
        first_votes = scores_firstvote[option_id]
        # Calculate the percentage of first votes for the current option
        first_vote_percentage = (first_votes / total_first_votes) * 100 if total_first_votes > 0 else 0

        borda_score = scores_borda[option_id]

        # Check if current option_id is in elimination candidates and apply red color if so
        if elimination_candidates and option_id in elimination_candidates:
            bezeichnung = RED + bezeichnung + RESET

            if mode == 'TVR':
                borda_score = RED + str(borda_score) + RESET
            elif mode == 'IRV':
                first_votes = RED + str(first_votes) + RESET

        table.append([round_no,
                      option_id,
                      bezeichnung,
                      first_votes,
                      first_vote_percentage,
                      borda_score])

    print(tabulate(table, headers=headers, tablefmt="pipe", numalign="right", floatfmt="2.2f"))

def print_ballots(round_no, stimmzettel, programme):
    headers = ['R', 'Ballot ID', 'C', 'Candidate', '#']
    table = []

    for voter_id, ballot in stimmzettel.items():
        for rank, single_vote in enumerate(ballot):
            table.append([round_no,
                          voter_id,
                          single_vote[1],
                          programme[single_vote[1]],
                          rank])

    print(tabulate(table, headers=headers, tablefmt="pipe", numalign="right"))

def runoff_round(round_no, programme, stimmzettel, scoring_function, results = set(), prior_eliminations = []):
    """Rekursives Runoff Voting.
    :param programme: Dict in Form {ProgrammID: Programm_Name}.
    :param stimmzettel: Dict in Form {'VoterID': [(Rang, ProgrammID)]}.
    :param scoring_function: Nimmt Programme und Stimmzettel und berechnet Scores als: {ProgrammID: Score}.
    :param results: Ausschliesslich rekursives Rueckgabeargument. Sollte nicht manuell gesetzt werden.
    :param prior_eliminations: Ausschliesslich rekursives Argument. Sollte nicht manuell gesetzt werden.
    :returns: Set in Form: {(WinnerID, EliminierungsReihenfolge)}. Beides Tupel.
    """

    if bool(os.getenv('PRINT_BALLOTS')):
        print_ballots(round_no, stimmzettel, programme)

    # Rekursive Abbruchbedingung.
    if len(programme) == 1:

        # Alles als Tupel, denn Listen sind nicht Hashable und Ergebnis ist Set um Duplikate zu vermeiden.
        results.add((list(programme.keys())[0], tuple(prior_eliminations)))
        return results

    # Berechnet die Erststimmen-Scores fuer diese Runde.
    erststimmen_scores = calculate_erststimmen_score(programme, stimmzettel)

    # Berechnet Scores und findet die Programm mit kleinstem Score, die eliminiert werden koennen.
    scores = scoring_function(programme, stimmzettel)
    minimum_score = min(scores.values())
    elimination_candidates = [programm for programm, score in scores.items() if score == minimum_score]

    borda_scores = calculate_borda_score(programme, stimmzettel)
    print_scores_table(erststimmen_scores, borda_scores, programme, round_no, elimination_candidates)
    print("Elimination Candidates at round no", round_no, ":", elimination_candidates, '\n')

    # Ueberprueft, ob ein Kandidat mehr als die Haelfte der Erststimmen hat.
    total_first_votes = sum(erststimmen_scores.values())
    for candidate, votes in erststimmen_scores.items():
        if votes > total_first_votes / 2:
            results.add((candidate, tuple(prior_eliminations)))
            return results

    # Jede moegliche Eliminationsreihenfolge wird getestet.
    for candidate in elimination_candidates:
        new_eliminations = prior_eliminations + [candidate]
        
        # Eliminierte Programme scheiden aus und werden von Stimmzetteln gestrichen.
        programme_reduziert = {p_id:name for (p_id, name) in programme.items() if p_id not in new_eliminations}
        new_stimmzettel = {z_id:[(r, s) for r, s in ranking if s not in new_eliminations] for (z_id, ranking) in stimmzettel.items()}

        # Neues Ergebnis wird mit bisherigen fuer Rueckgabe vereinigt.
        results.union(runoff_round(round_no + 1, programme_reduziert, new_stimmzettel, scoring_function, results, new_eliminations))
    
    return results

def main(argv):

    # I/O
    mode_scoring_funcs = {"IRV":calculate_erststimmen_score, "TVR": calculate_borda_score}

    inputfile = ''
    global mode
    opts, args = getopt.getopt(argv,"hi:m:",["inputfile="])
    for opt, arg in opts:
        if opt == '-h':
            print ('test.py -i <inputfile> -m <mode>'
                   + '\nAvailable modes are: '+ (', '.join(list(mode_scoring_funcs.keys())))
                   + '. Default mode is ' + DEFAULT_MODE + '.')
            sys.exit()
        elif opt in ("-i", "--inputfile"):
            if os.path.isfile(arg):
                inputfile = arg
            else:
                print("File does not exist: " + arg)
                sys.exit()
        elif opt in ("-m", "--mode"):
            if arg in mode_scoring_funcs.keys():
                mode = arg
            else:
                print("Unknown mode: " + arg)
                sys.exit()

    # LOAD
    if inputfile != '':
        data = pd.read_csv(inputfile, delimiter=';', encoding = "UTF-8")
    else: 
        print("No input file provided. See -h for help.")
        sys.exit()
        
    print ('Input file is ', inputfile)
    print ('Mode is ', mode if mode != '' else DEFAULT_MODE)

    # INIT
    programmids = {}
    stimmzettel = {}
    scoring_func = mode_scoring_funcs[mode] if mode != '' else mode_scoring_funcs[DEFAULT_MODE]

    # Zeilenindex wird nicht verwendet.
    for _, row in data.iterrows():

        # Dict in form: {ProgrammID: Programm_Name}.
        programmids[row["optionId"]] = row["bezeichnung"]

        #Stimzettel werden dicts wie folgt: {'VoterID': [(Rang, ProgrammID)]}.
        if not row["hashvoter"] in stimmzettel:
            stimmzettel[row["hashvoter"]] = []
        stimmzettel[row["hashvoter"]].append((row["rank"], row["optionId"]))

    # Rekursives Voting.
    all_executions = runoff_round(1, programmids, stimmzettel, scoring_func)

    # Zeige alle Executions mit Eliminationsreihenfolgen.
    for execution in all_executions:
        print("Winner: ", execution[0], "Elimination order: ", execution[1])

    # Set mit Siegern der Wahl allen Eliminationsreihenfolgen ohne Duplikate.
    winners = list({winner for (winner, _) in all_executions})

    # Es sollte nur einen eindeutigen Sieger geben. 
    # Falls das nicht der Fall ist, muessen Ties manuell gebrochen werden.
    if len(winners) > 1:
        print("WARNING: Winners change depending on arbitrary elimination order")
    else:
        print("Winner: ", programmids[winners[0]])

if __name__ == "__main__":
   main(sys.argv[1:])
